module template-html

go 1.14

require (
	github.com/gofiber/fiber/v2 v2.0.2
	github.com/gofiber/template v1.6.2
)
